# CI : Lint Markdown

Will lint every .md files in project using NodeJS package markdownlint-cli.

[markdownlint-cli documentation](https://github.com/igorshubovych/markdownlint-cli)

## Usage

```yaml
include:
  - project: 'bibliosansfrontieres/ci/lint/markdown'
    file: 'template.yml'
```

## Override variables

|Variable|Description|Default|Required|
|-|-|-|-|
|MARKDOWN_FILES_TO_EXCLUDE|Files you want to exclude from lint| |no|
|MARKDOWN_FILES_TO_LINT|Specific files you want to lint| |no|

## Choose stage

By default, `lint_markdown` uses `lint` stage.

You can override this by adding following code to your `.gitlab-ci.yml` :

```yaml
lint_markdown:
  stage: THE_STAGE_YOU_WANT_TO_USE
```

## Use locally

Copy compose.yml to your project.

Then run :

```shell
docker compose up --pull always
```

You can also rename `compose.yml` to any name you prefer `ci.yml` for example.

Then run :

```shell
docker compose -f ci.yml up --pull always
```
