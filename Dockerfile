FROM node:20-alpine3.19

RUN npm i -g markdownlint-cli

WORKDIR /markdownlint

COPY entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]