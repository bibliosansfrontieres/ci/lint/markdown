#!/bin/sh
cd /markdown

if [ -n "$MARKDOWN_FILES_TO_EXCLUDE" ] && [ -n "$MARKDOWN_FILES_TO_LINT" ]; then
    echo "Error: Both MARKDOWN_FILES_TO_EXCLUDE and MARKDOWN_FILES_TO_LINT cannot be filled simultaneously."
    exit 1
fi

exclude_option=""
if [ -n "$MARKDOWN_FILES_TO_EXCLUDE" ]; then
    for file in $MARKDOWN_FILES_TO_EXCLUDE; do
        exclude_option="${exclude_option}--ignore $file "
    done
fi

files_to_lint=""
if [ -n "$MARKDOWN_FILES_TO_LINT" ]; then
    echo "Linting specific files using MARKDOWN_FILES_TO_LINT variable"
    for file in $MARKDOWN_FILES_TO_LINT; do
        echo "Linting $file"
    done
    files_to_lint="$MARKDOWN_FILES_TO_LINT"
else
    echo "Linting all Markdown files in the repository"
    all_md_files=$(find . -name "*.md")
    excluded_md_files="$all_md_files"
    if [ -n "$MARKDOWN_FILES_TO_EXCLUDE" ]; then
      excluded_md_files=$(echo "$all_md_files" | grep -vE "$(echo "$MARKDOWN_FILES_TO_EXCLUDE" | tr ' ' '|')")
    fi
    for file in $excluded_md_files; do
        echo "Linting $file"
    done
    files_to_lint="**/*.md"
fi


markdownlint $exclude_option $files_to_lint